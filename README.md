# Resources
- [Tutorial Edge - Go Websocket Tutorial](https://tutorialedge.net/golang/go-websocket-tutorial/)
- [Tutorial Edge - Course: Building a Chat Application in Go with ReactJS](https://tutorialedge.net/projects/chat-system-in-go-and-react/)
- [A Million WebSockets and Go (Sergey Kamardin, 2017)](https://www.freecodecamp.org/news/million-websockets-and-go-cc58418460bb/ )
- Going Infinite, handling 1 millions websockets connections in Go / Eran Yanay
    - [Video](https://www.youtube.com/watch?v=LI1YTFMi8W4)
    - [Slides](https://speakerdeck.com/gopherconil/eran-yanay)
    - [Code](https://github.com/eranyanay/1m-go-websockets)

- https://caniuse.com/#feat=websockets